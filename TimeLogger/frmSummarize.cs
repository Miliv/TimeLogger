﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace TimeLogger {
	public partial class frmSummarize : Form {
		public frmSummarize(string summary) {
			InitializeComponent();

			webBrowser1.DocumentText = summary;
			//Acá lo que tendría que hacer para los links es:
			//System.Diagnostics.Process.Start("www.google.com");
		}
	}
}

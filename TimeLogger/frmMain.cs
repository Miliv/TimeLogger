﻿using Microsoft.Win32;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Windows.Forms;

namespace TimeLogger {
	public partial class FrmMain : Form {
		private bool SHIFT = false;
		private bool LOADING = false;
		private DateTime _leaveTime;

		AutoCompleteStringCollection autoComplete = new AutoCompleteStringCollection();
		int rows;
		int LINE_HEIGHT;
		List<TextBox> tasks;// = new List<TextBox>();      //Task #1 = 0
		List<TextBox> times;// = new List<TextBox>();      //Task #1's Time = 1, Ini Time = 0
		List<Label> spents;// = new List<Label>();      //Task #1 = 0
		List<Label> totals;// = new List<Label>();      //Task #1 = 0
		private const string ALMUERZO = "Almuerzo";
		private string AUX_TEXT;


		public FrmMain() {
			InitializeComponent();
			//SystemEvents.SessionSwitch += new SessionSwitchEventHandler(SystemEvents_SessionSwitch);
			SystemEvents.SessionSwitch += SystemEvents_SessionSwitch;
		}

		protected override void OnLoad(EventArgs e) {
			this.txtTask.AutoCompleteCustomSource = autoComplete;
			LINE_HEIGHT = txtTime.Height + txtTime.Margin.Top;

			this.lblDate.Text = DateTime.Now.ToString("yyyyMMdd");

			string todayFile = DayFilePath();
			if (File.Exists(todayFile)) {
				LoadFromFile(todayFile);
			} else {
				InitializeForm();
				txtIniTime.Text = DateTime.Now.ToString("HH:mm");
				//txtTask.Select();
			}
		}

		protected override void OnResize(EventArgs e) {
			base.OnResize(e);
			if (FormWindowState.Minimized == this.WindowState) {
				Persist(false);
				notifyIcon.Visible = true;
				notifyIcon.Text = "Current Task: " + tasks.Last().Text;
				//notifyIcon.ShowBalloonTip(500);
				this.Hide();
			} else if (FormWindowState.Normal == this.WindowState) {
				notifyIcon.Visible = false;
			}
		}

		protected override void OnFormClosing(FormClosingEventArgs e) {
			DialogResult res = MessageBox.Show("Yes = Save & Exit \n No = Exit \n Cancel = Stay?", "Quit", MessageBoxButtons.YesNoCancel, MessageBoxIcon.Question);

			if (res == DialogResult.Cancel) {
				e.Cancel = true;
			} else { 
				if (res == DialogResult.Yes) {
					Persist(false);
				}
				//SystemEvents.SessionSwitch -= new SessionSwitchEventHandler(SystemEvents_SessionSwitch);
				SystemEvents.SessionSwitch -= SystemEvents_SessionSwitch;
			}
		}

		// unhook static eventhandler when application terminates!

		//Handle event
		protected void SystemEvents_SessionSwitch(object sender, SessionSwitchEventArgs e) {
			Console.WriteLine(e.Reason.ToString());
			if (e.Reason == SessionSwitchReason.SessionLock) {
				_leaveTime = DateTime.Now;
			} else if (e.Reason == SessionSwitchReason.SessionUnlock) {
				//I returned to my desk
				RestoreForm();
				times.Last().Focus();
				/*
				if (MessageBox.Show("¿Te olvidaste de anotar antes de irte?"
									, "Lock", MessageBoxButtons.YesNo
									, MessageBoxIcon.Question
									, MessageBoxDefaultButton.Button2) == DialogResult.Yes) {
					if (_leaveTime != null) {

					}
				}
				*/
			}
		}

		private void InitializeForm() {
			rows = 1;
			tasks = new List<TextBox>();
			times = new List<TextBox>();
			spents = new List<Label>();
			totals = new List<Label>();

			tasks.Add(this.txtTask);
			times.Add(this.txtIniTime);
			times.Add(this.txtTime);
			spents.Add(this.lblSpent);
			totals.Add(this.lblTotal);

			this.Height = 139;
		}

		private void ClearForm() {
			for (int i = this.Controls.Count - 1; i >= 0; i--) {
				if (this.Controls[i].Name.StartsWith("new")) this.Controls[i].Dispose();
			}
			/*
			foreach (Control c in this.Controls) {
				if (c.GetType().Equals(typeof(TextBox)) 
					|| c.GetType().Equals(typeof(Label))) {
					c.Text = "";
				}
			}
			*/
			txtIniTime.Text = "";
			txtTask.Text = "";
			txtTime.Text = "";
			lblSpent.Text = "00:00";
			lblTotal.Text = "00:00";
			InitializeForm();
		}



		private void CmdAdd_Click(object sender, EventArgs e) {
			TextBox lastTask = tasks.Last();
			if (!LOADING) { 
				if (lastTask.Text.Equals("")) {
					lastTask.Focus();
					return;
				}
			}
			TextBox lastTime = times.Last();
			if (!LOADING) { 
				if (lastTime.Text.Equals("")) {
					lastTime.Text = DateTime.Now.ToString("HH:mm");
				}
			}
			bool free = (Control.ModifierKeys == Keys.Shift || SHIFT);
			bool repeatLast = (Control.ModifierKeys == Keys.Control);

			CreateNewTaskBox(free, repeatLast);
			CreateNewTimeBox(free);
			if (!free) {
				if (showChekboxesMI.Checked) CreateNewCheckLogged();
			}
			CreateNewLabel(this.lblSpent);
			CreateNewLabel(this.lblTotal);

			if (!LOADING) { 
				if (free) {
					times.Last().Focus();
				} else {
					tasks.Last().Focus();
				}

				CalcTime();
			}
			this.Height += LINE_HEIGHT; //Because of cmdAdd's anchor, there's no need to relocate it
			rows++;
		}

		private void CreateNewTaskBox(bool free, bool repeatLast) {
			TextBox txt = new TextBox();
			txt.Name = "newTxtTask" + rows;
			txt.Tag = (free ? "Free" : "Task") /*+ rows*/;
			int y = (LINE_HEIGHT * rows);
			txt.Location = new Point(txtTask.Location.X, txtTask.Location.Y + y);
			txt.Size = txtTask.Size;
			txt.Anchor = txtTask.Anchor;
			txt.AutoCompleteMode = txtTask.AutoCompleteMode;
			txt.AutoCompleteSource = txtTask.AutoCompleteSource;
			txt.AutoCompleteCustomSource = autoComplete;
			txt.GotFocus += new EventHandler(this.Task_GotFocus);
			txt.LostFocus += new EventHandler(this.Task_LostFocus);
			txt.TabIndex = (rows * 2) + 1; //cmdAdd.TabIndex;
			if (free) {
				txt.Text = ALMUERZO;
				txt.BackColor = Color.FromArgb(247, 255, 204);
			} else {
				if (tasks.Last().Text.Equals(ALMUERZO) || repeatLast) {
					TextBox ant = tasks.ElementAtOrDefault(tasks.Count - 2);
					txt.Text = (ant != null ? ant.Text : "");
				}
			}

			this.Controls.Add(txt);
			tasks.Add(txt);
		}

		private void CreateNewTimeBox(bool free) {
			TextBox txt = new TextBox();
			txt.Name = "newTxtTime" + rows;
			txt.Tag = (free ? "Free" : "Time") /*+ rows*/;
			int y = (LINE_HEIGHT * rows);
			txt.Location = new Point(txtTime.Location.X, txtTime.Location.Y + y);
			txt.Size = txtTime.Size;
			txt.Anchor = txtTime.Anchor;
			txt.GotFocus += new EventHandler(this.Time_GotFocus);
			txt.LostFocus += new EventHandler(this.Time_LostFocus);
			txt.KeyPress += new KeyPressEventHandler(this.TextBox_OnlyTime);
			txt.TabIndex = (rows * 2) + 2; //cmdAdd.TabIndex;
			this.Controls.Add(txt);
			times.Add(txt);
		}

		private void CreateNewCheckLogged() {
			CheckBox chk = new CheckBox();
			chk.Name = "newChkLog" + rows;
			int y = (LINE_HEIGHT * rows);
			chk.Location = new Point(chkLogged.Location.X, chkLogged.Location.Y + y);
			chk.Size = chkLogged.Size;
			chk.Anchor = chkLogged.Anchor;
			this.Controls.Add(chk);
		}

		private void CreateNewLabel(Label original) {
			Label lbl = new Label();
			lbl.Name = "new" + original.Name + rows;
			int y = (LINE_HEIGHT * rows);
			lbl.Location = new Point(original.Location.X, original.Location.Y + y);
			lbl.Size = original.Size;
			lbl.Anchor = original.Anchor;
			lbl.Text = "00:00";
			this.Controls.Add(lbl);
			if (original.Name == lblSpent.Name) {
				spents.Add(lbl);
			} else if (original.Name == lblTotal.Name) {
				totals.Add(lbl);
			}
		}

		private void Task_GotFocus(object sender, EventArgs e) {
			TextBox t = (TextBox)sender;
			AUX_TEXT = t.Text;
		}
		private void Task_LostFocus(object sender, EventArgs e) {
			TextBox txt = (TextBox)sender;
			if (txt.Text.Equals(AUX_TEXT)) return;

			if (suggestOldTasksMI.Checked) {
				if (txt.Text != string.Empty) autoComplete.Add(txt.Text);
			} else {
				autoComplete.Clear();
				foreach (TextBox t in this.Controls.OfType<TextBox>().Where<TextBox>(t => t.Tag != null && t.Tag.ToString().Substring(0, 4) == "Task")) {
					if (t.Text != string.Empty) autoComplete.Add(t.Text);
				}
			}
			this.Refresh();
		}

		private void Time_GotFocus(object sender, EventArgs e) {
			TextBox t = (TextBox)sender;
			t.Text = t.Text.Replace(":", "");
			t.SelectAll();
		}

		private void Time_LostFocus(object sender, EventArgs e) {
			TextBox t = (TextBox)sender;
			if (t.Text.IndexOf(":") >= 0) return;

			if (t.Text.Length == 0) {
				return;
			} else if (t.Text.Length == 1) {
				t.Text = "0" + t.Text + ":00";
			} else if (t.Text.Length == 2) {
				t.Text = t.Text + ":00";
			} else if (t.Text.Length == 3) {
				t.Text = "0" + t.Text.Substring(0, 1) + ":" + t.Text.Substring(1, 2);
			} else if (t.Text.Length == 4) {
				t.Text = t.Text.Substring(0, 2) + ":" + t.Text.Substring(2, 2);
			} else {
				t.Focus();
			}
			t.Refresh();
			CalcTime();
		}

		private void TextBox_OnlyTime(object sender, KeyPressEventArgs e) {
			if (!char.IsControl(e.KeyChar)
				&& !char.IsDigit(e.KeyChar)
				&& e.KeyChar != ':') {
				e.Handled = true;
			}
		}

		private void CalcTime() {
			TimeSpan total = new TimeSpan(0);
			TimeSpan totalFree = new TimeSpan(0);
			for (int i = 1; i < times.Count; i++) {
				TimeSpan timeEnd = Time(times[i].Text);
				TimeSpan timeStart = Time(times[i - 1].Text);
				TimeSpan spent = new TimeSpan(0);
				if (timeStart.TotalMinutes > 0 && timeEnd.TotalMinutes > 0) {
					spent = timeEnd.Subtract(timeStart);
				}
				spents[i - 1].Text = FormatTime(spent);
				total = total.Add(spent);
				totals[i - 1].Text = FormatTime(total);

				if (tasks[i-1].Tag.ToString() == "Free") {
					totalFree = totalFree.Add(spent);
				}
			}

			lblEndTime.Text = FormatTime(Time(txtIniTime.Text).Add(TimeSpan.FromHours(8)).Add(totalFree));
		}


		private TimeSpan Time(string time) {
			TimeSpan ret = new TimeSpan(0);
			if (time == "") return ret;
			if (time.IndexOf(":") == -1) return ret;
			string[] aux = time.Split(':');
			return new TimeSpan(int.Parse(aux[0]), int.Parse(aux[1]), 0);
		}
		private string FormatTime(TimeSpan t) {
			return t.Hours.ToString().PadLeft(2, '0') + ":" + t.Minutes.ToString().PadLeft(2, '0');
		}
		private void alwaysOnTopToolStripMenuItem_Click(object sender, EventArgs e) {
			this.TopMost = alwaysOnTopMI.Checked;
		}
		private int TotalMinutes(string time) {
			return int.Parse(Time(time).TotalMinutes.ToString());
		}


		private void summarizeToolStripMenuItem_Click(object sender, EventArgs e) {
			cmdAdd.Focus();
			CalcTime();

			Dictionary<string, Tuple<int, int>> dic = buildDic();

			string res = "<style>.mono {font-family: \"Courier New\", monospace;}</style><body class=\"mono\">";
			res += "Ini: " + txtIniTime.Text + /*'\n'*/"<br/>";
			res += new String('-', 56) + /*'\n'*/"<br/>";

			int total = 0;
			foreach (KeyValuePair<string, Tuple<int, int>> x in dic) {
				if (x.Value.Item2 == 0) {
					if (x.Key.Contains("?")) {
						res += "<a href=\"https://oxecon.atlassian.net/browse/" + x.Key + "\" target=\"_blank\">";
					}
					res += x.Key;
					if (x.Key.Contains("?")) {
						res += "</a>";
					}
					res += ": " + new String('!', 35 - x.Key.Length).Replace("!", "&nbsp;");

					res += (x.Value.Item1 + " mins").PadLeft(8, '$').Replace("$", "&nbsp;")
						+ "(" + HoursMins(x.Value.Item1) + ")" + /*'\n'*/"<br/>";
					total += x.Value.Item1;
				}
			}
			res += new String('-', 56) + "<br/>";
			res += "Total: " + total + " mins (" + HoursMins(total) + ")<br/>";

			res += /*'\n'*/"<br/>";

			int totalFree = 0;
			foreach (KeyValuePair<string, Tuple<int, int>> x in dic) {
				if (x.Value.Item2 == 1) {
					res += x.Key.PadRight(40) + ": "
					+ (x.Value.Item1 + " mins").PadLeft(8, '!').Replace("!", "&nbsp;")
					+ '\t' + "(" + HoursMins(x.Value.Item1) + ")" + /*'\n'*/"<br/>";
					totalFree += x.Value.Item1;
				}
			}
			res += new String('-', 56) + "<br/>";
			res += "Total: " + totalFree + " mins (" + HoursMins(totalFree) + " )<br/>";

			res += /*'\n'*/"<br/>";


			res += "Fin: " + Time(txtIniTime.Text).Add(TimeSpan.FromHours(8)).Add(TimeSpan.FromMinutes(totalFree)).ToString();

			res += "</body>";

			Form m = new frmSummarize(res);
			m.ShowDialog();

			//res += /*'\n'*/"<br/>" + /*'\n'*/"<br/>" + "http://www.google.com";
			//MessageBox.Show(res);
		}
			
		private string HoursMins(int mins) {
			double hours = Math.Round(mins / 60.00, 2);
			double fullHours = Math.Floor(hours);
			string ret = "";
			
			if (fullHours > 0) ret += fullHours + "h";

			if (hours != fullHours) {
				if (ret != "") ret += " ";
				ret += (mins - (fullHours * 60)) + "m";
			}

			return ret;
		}


		private void dummyToolStripMenuItem_Click(object sender, EventArgs e) {
			txtIniTime.Text = "09:00";
			txtTask.Text = "HALO-550";
			txtTime.Text = "10:30";

			CmdAdd_Click(this, e);
			tasks.Last().Text = "HALO-551";
			times.Last().Text = "12:45";

			SHIFT = true;
			CmdAdd_Click(this, e);
			times.Last().Text = "13:15";

			SHIFT = false;
			CmdAdd_Click(this, e);
			tasks.Last().Text = "HALO-551";
			times.Last().Text = "14:15";

			CmdAdd_Click(this, e);
			tasks.Last().Text = "HALO-552";
			times.Last().Text = "16:30";

			CmdAdd_Click(this, e);
			tasks.Last().Text = "HALO-553";
			times.Last().Text = "18:00";

			CalcTime();
		}

		private void showChekboxesToolStripMenuItem_Click(object sender, EventArgs e) {
			chkLogged.Visible = showChekboxesMI.Checked;
		}

		private Dictionary<string, Tuple<int, int>> buildDic() {
			Dictionary<string, Tuple<int, int>> dic = new Dictionary<string, Tuple<int, int>>();

			for (int i = 0; i < tasks.Count; i++) {
				string task = tasks[i].Text;
				int time = TotalMinutes(spents[i].Text);

				if (dic.ContainsKey(task)) {
					dic[task] = new Tuple<int, int>(dic[task].Item1 + time, dic[task].Item2);
				} else {
					dic.Add(task, new Tuple<int, int>(time, (tasks[i].Tag.ToString() == "Free" ? 1 : 0)));
				}
			}

			return dic;
		}

		private void Persist(bool msg) {
			List<Tuple<string, string, int>> list = new List<Tuple<string, string, int>>();
			
			for (int i = 0; i < tasks.Count; i++) {
				if (tasks[i].Text != "" && times[i + 1].Text != "") {
					list.Add(new Tuple<string, string, int>(tasks[i].Text, times[i + 1].Text, (tasks[i].Tag.ToString() == "Free" ? 1 : 0)));
				}				
			}

			object o = new { Ini = txtIniTime.Text, Date = lblDate.Text, List = list };			
			string json = JsonConvert.SerializeObject(o, Formatting.Indented);

			using (StreamWriter writetext = File.CreateText(DayFilePath())) {
				writetext.WriteLine(json);
			}

			if (msg) MessageBox.Show("Save OK");
		}
		
		private void saveMI_Click(object sender, EventArgs e) {
			Persist(true);
		}

		private void loadMI_Click(object sender, EventArgs e) {
			this.openFileDialog.ShowDialog();
			string file = this.openFileDialog.FileName;
			if (file == "") return;

			LoadFromFile(file);			
		}

		private string DayFilePath() {
			string path = AppDomain.CurrentDomain.BaseDirectory;
			if (path.Substring(path.Length) != "\\") path += "\\";
			return path + "TimeLog_" + lblDate.Text + ".json";
		}

		private void LoadFromFile(string file) {
			LOADING = true;
			ClearForm();

			StreamReader r = new StreamReader(file);
			dynamic json = JsonConvert.DeserializeObject(r.ReadToEnd());
			r.Close();
			txtIniTime.Text = json.Ini;
			lblDate.Text = json.Date;


			bool first = true;
			foreach (dynamic o in json.List) {
				SHIFT = (o.Item3 == 1);
				if (!first) {
					CmdAdd_Click(this, null);
				} else {
					first = false;
				}

				tasks.Last().Text = o.Item1;
				times.Last().Text = o.Item2;
				//times.Last().Text = FormatTime(Time(times[rows - 1].Text).Add(TimeSpan.FromMinutes(Convert.ToDouble(o.Value.Item1))));
			}
			CalcTime();
			times.Last().Focus();
			LOADING = false;			
		}

		private void notifyIcon_MouseDoubleClick(object sender, MouseEventArgs e) {
			RestoreForm();
		}
		private void notifyIcon_MouseClick(object sender, MouseEventArgs e) {
			RestoreForm();
		}

		private void RestoreForm() {
			this.Show();
			this.WindowState = FormWindowState.Normal;
		}

		private void getLastLockTimeMI_Click(object sender, EventArgs e) {
			MessageBox.Show(_leaveTime == null ? "No info" : _leaveTime.ToShortTimeString());
		}
	}
}

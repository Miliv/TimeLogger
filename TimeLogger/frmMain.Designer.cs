﻿namespace TimeLogger
{
	partial class FrmMain
	{
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.IContainer components = null;

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		/// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
		protected override void Dispose(bool disposing)
		{
			if (disposing && (components != null))
			{
				components.Dispose();
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code

		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.components = new System.ComponentModel.Container();
			System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FrmMain));
			this.label1 = new System.Windows.Forms.Label();
			this.txtIniTime = new System.Windows.Forms.TextBox();
			this.txtTask = new System.Windows.Forms.TextBox();
			this.txtTime = new System.Windows.Forms.TextBox();
			this.chkLogged = new System.Windows.Forms.CheckBox();
			this.cmdAdd = new System.Windows.Forms.Button();
			this.lblSpent = new System.Windows.Forms.Label();
			this.label3 = new System.Windows.Forms.Label();
			this.label4 = new System.Windows.Forms.Label();
			this.lblTotal = new System.Windows.Forms.Label();
			this.contextMenu = new System.Windows.Forms.ContextMenuStrip(this.components);
			this.summarizeMI = new System.Windows.Forms.ToolStripMenuItem();
			this.alwaysOnTopMI = new System.Windows.Forms.ToolStripMenuItem();
			this.suggestOldTasksMI = new System.Windows.Forms.ToolStripMenuItem();
			this.showChekboxesMI = new System.Windows.Forms.ToolStripMenuItem();
			this.dummyToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
			this.persistanceMI = new System.Windows.Forms.ToolStripMenuItem();
			this.loadMI = new System.Windows.Forms.ToolStripMenuItem();
			this.saveMI = new System.Windows.Forms.ToolStripMenuItem();
			this.openFileDialog = new System.Windows.Forms.OpenFileDialog();
			this.notifyIcon = new System.Windows.Forms.NotifyIcon(this.components);
			this.lblDate = new System.Windows.Forms.Label();
			this.lblEndTime = new System.Windows.Forms.Label();
			this.label2 = new System.Windows.Forms.Label();
			this.getLastLockTimeMI = new System.Windows.Forms.ToolStripMenuItem();
			this.contextMenu.SuspendLayout();
			this.SuspendLayout();
			// 
			// label1
			// 
			this.label1.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
			this.label1.AutoSize = true;
			this.label1.Location = new System.Drawing.Point(213, 8);
			this.label1.Margin = new System.Windows.Forms.Padding(4);
			this.label1.Name = "label1";
			this.label1.Size = new System.Drawing.Size(25, 20);
			this.label1.TabIndex = 0;
			this.label1.Text = "Ini";
			// 
			// txtIniTime
			// 
			this.txtIniTime.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
			this.txtIniTime.Location = new System.Drawing.Point(245, 5);
			this.txtIniTime.Name = "txtIniTime";
			this.txtIniTime.Size = new System.Drawing.Size(64, 25);
			this.txtIniTime.TabIndex = 0;
			this.txtIniTime.GotFocus += new System.EventHandler(this.Time_GotFocus);
			this.txtIniTime.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.TextBox_OnlyTime);
			this.txtIniTime.LostFocus += new System.EventHandler(this.Time_LostFocus);
			// 
			// txtTask
			// 
			this.txtTask.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
			this.txtTask.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
			this.txtTask.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.CustomSource;
			this.txtTask.Location = new System.Drawing.Point(16, 39);
			this.txtTask.MaxLength = 35;
			this.txtTask.Name = "txtTask";
			this.txtTask.Size = new System.Drawing.Size(223, 25);
			this.txtTask.TabIndex = 1;
			this.txtTask.Tag = "Task";
			this.txtTask.GotFocus += new System.EventHandler(this.Task_GotFocus);
			this.txtTask.LostFocus += new System.EventHandler(this.Task_LostFocus);
			// 
			// txtTime
			// 
			this.txtTime.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
			this.txtTime.Location = new System.Drawing.Point(245, 39);
			this.txtTime.Name = "txtTime";
			this.txtTime.Size = new System.Drawing.Size(64, 25);
			this.txtTime.TabIndex = 2;
			this.txtTime.Tag = "Time";
			this.txtTime.GotFocus += new System.EventHandler(this.Time_GotFocus);
			this.txtTime.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.TextBox_OnlyTime);
			this.txtTime.LostFocus += new System.EventHandler(this.Time_LostFocus);
			// 
			// chkLogged
			// 
			this.chkLogged.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
			this.chkLogged.AutoSize = true;
			this.chkLogged.Location = new System.Drawing.Point(434, 46);
			this.chkLogged.Name = "chkLogged";
			this.chkLogged.Size = new System.Drawing.Size(15, 14);
			this.chkLogged.TabIndex = 4;
			this.chkLogged.UseVisualStyleBackColor = true;
			this.chkLogged.Visible = false;
			// 
			// cmdAdd
			// 
			this.cmdAdd.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
			this.cmdAdd.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.cmdAdd.Location = new System.Drawing.Point(16, 70);
			this.cmdAdd.Name = "cmdAdd";
			this.cmdAdd.Size = new System.Drawing.Size(19, 23);
			this.cmdAdd.TabIndex = 3;
			this.cmdAdd.Text = "+";
			this.cmdAdd.UseVisualStyleBackColor = true;
			this.cmdAdd.Click += new System.EventHandler(this.CmdAdd_Click);
			// 
			// lblSpent
			// 
			this.lblSpent.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
			this.lblSpent.Location = new System.Drawing.Point(316, 42);
			this.lblSpent.Margin = new System.Windows.Forms.Padding(4);
			this.lblSpent.Name = "lblSpent";
			this.lblSpent.Size = new System.Drawing.Size(47, 20);
			this.lblSpent.TabIndex = 6;
			this.lblSpent.Text = "00:00";
			// 
			// label3
			// 
			this.label3.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
			this.label3.Location = new System.Drawing.Point(316, 8);
			this.label3.Margin = new System.Windows.Forms.Padding(4);
			this.label3.Name = "label3";
			this.label3.Size = new System.Drawing.Size(47, 20);
			this.label3.TabIndex = 7;
			this.label3.Text = "Spent";
			// 
			// label4
			// 
			this.label4.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
			this.label4.Location = new System.Drawing.Point(371, 8);
			this.label4.Margin = new System.Windows.Forms.Padding(4);
			this.label4.Name = "label4";
			this.label4.Size = new System.Drawing.Size(47, 20);
			this.label4.TabIndex = 8;
			this.label4.Text = "Total";
			// 
			// lblTotal
			// 
			this.lblTotal.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
			this.lblTotal.Location = new System.Drawing.Point(371, 42);
			this.lblTotal.Margin = new System.Windows.Forms.Padding(4);
			this.lblTotal.Name = "lblTotal";
			this.lblTotal.Size = new System.Drawing.Size(47, 20);
			this.lblTotal.TabIndex = 9;
			this.lblTotal.Text = "00:00";
			// 
			// contextMenu
			// 
			this.contextMenu.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.summarizeMI,
            this.alwaysOnTopMI,
            this.suggestOldTasksMI,
            this.showChekboxesMI,
            this.dummyToolStripMenuItem,
            this.persistanceMI,
            this.getLastLockTimeMI});
			this.contextMenu.Name = "contextMenu";
			this.contextMenu.Size = new System.Drawing.Size(175, 180);
			// 
			// summarizeMI
			// 
			this.summarizeMI.Name = "summarizeMI";
			this.summarizeMI.Size = new System.Drawing.Size(174, 22);
			this.summarizeMI.Text = "Summarize";
			this.summarizeMI.Click += new System.EventHandler(this.summarizeToolStripMenuItem_Click);
			// 
			// alwaysOnTopMI
			// 
			this.alwaysOnTopMI.CheckOnClick = true;
			this.alwaysOnTopMI.Name = "alwaysOnTopMI";
			this.alwaysOnTopMI.Size = new System.Drawing.Size(174, 22);
			this.alwaysOnTopMI.Text = "Always on top";
			this.alwaysOnTopMI.Click += new System.EventHandler(this.alwaysOnTopToolStripMenuItem_Click);
			// 
			// suggestOldTasksMI
			// 
			this.suggestOldTasksMI.Checked = true;
			this.suggestOldTasksMI.CheckState = System.Windows.Forms.CheckState.Checked;
			this.suggestOldTasksMI.Name = "suggestOldTasksMI";
			this.suggestOldTasksMI.Size = new System.Drawing.Size(174, 22);
			this.suggestOldTasksMI.Text = "Suggest old tasks";
			// 
			// showChekboxesMI
			// 
			this.showChekboxesMI.CheckOnClick = true;
			this.showChekboxesMI.Name = "showChekboxesMI";
			this.showChekboxesMI.Size = new System.Drawing.Size(174, 22);
			this.showChekboxesMI.Text = "Show Chekboxes";
			this.showChekboxesMI.Click += new System.EventHandler(this.showChekboxesToolStripMenuItem_Click);
			// 
			// dummyToolStripMenuItem
			// 
			this.dummyToolStripMenuItem.Name = "dummyToolStripMenuItem";
			this.dummyToolStripMenuItem.Size = new System.Drawing.Size(174, 22);
			this.dummyToolStripMenuItem.Text = "Dummy";
			this.dummyToolStripMenuItem.Click += new System.EventHandler(this.dummyToolStripMenuItem_Click);
			// 
			// persistanceMI
			// 
			this.persistanceMI.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.loadMI,
            this.saveMI});
			this.persistanceMI.Name = "persistanceMI";
			this.persistanceMI.Size = new System.Drawing.Size(174, 22);
			this.persistanceMI.Text = "Persistance";
			// 
			// loadMI
			// 
			this.loadMI.Name = "loadMI";
			this.loadMI.Size = new System.Drawing.Size(152, 22);
			this.loadMI.Text = "Load";
			this.loadMI.Click += new System.EventHandler(this.loadMI_Click);
			// 
			// saveMI
			// 
			this.saveMI.Name = "saveMI";
			this.saveMI.Size = new System.Drawing.Size(152, 22);
			this.saveMI.Text = "Save";
			this.saveMI.Click += new System.EventHandler(this.saveMI_Click);
			// 
			// openFileDialog
			// 
			this.openFileDialog.Filter = "TimeLog files|*.json";
			// 
			// notifyIcon
			// 
			this.notifyIcon.BalloonTipIcon = System.Windows.Forms.ToolTipIcon.Info;
			this.notifyIcon.BalloonTipText = "TimeLogger is minimized...";
			this.notifyIcon.BalloonTipTitle = "TimeLogger";
			this.notifyIcon.Icon = ((System.Drawing.Icon)(resources.GetObject("notifyIcon.Icon")));
			this.notifyIcon.Text = "TimeLogger";
			this.notifyIcon.MouseClick += new System.Windows.Forms.MouseEventHandler(this.notifyIcon_MouseClick);
			this.notifyIcon.MouseDoubleClick += new System.Windows.Forms.MouseEventHandler(this.notifyIcon_MouseDoubleClick);
			// 
			// lblDate
			// 
			this.lblDate.AutoSize = true;
			this.lblDate.Location = new System.Drawing.Point(13, 8);
			this.lblDate.Margin = new System.Windows.Forms.Padding(4);
			this.lblDate.Name = "lblDate";
			this.lblDate.Size = new System.Drawing.Size(59, 20);
			this.lblDate.TabIndex = 10;
			this.lblDate.Text = "<DATE>";
			// 
			// lblEndTime
			// 
			this.lblEndTime.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
			this.lblEndTime.AutoSize = true;
			this.lblEndTime.Location = new System.Drawing.Point(250, 71);
			this.lblEndTime.Margin = new System.Windows.Forms.Padding(4);
			this.lblEndTime.Name = "lblEndTime";
			this.lblEndTime.Size = new System.Drawing.Size(47, 20);
			this.lblEndTime.TabIndex = 11;
			this.lblEndTime.Text = "00:00";
			// 
			// label2
			// 
			this.label2.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
			this.label2.AutoSize = true;
			this.label2.Location = new System.Drawing.Point(205, 71);
			this.label2.Margin = new System.Windows.Forms.Padding(4);
			this.label2.Name = "label2";
			this.label2.Size = new System.Drawing.Size(33, 20);
			this.label2.TabIndex = 12;
			this.label2.Text = "End";
			// 
			// getLastLockTimeMI
			// 
			this.getLastLockTimeMI.Name = "getLastLockTimeMI";
			this.getLastLockTimeMI.Size = new System.Drawing.Size(174, 22);
			this.getLastLockTimeMI.Text = "Get Last Lock Time";
			this.getLastLockTimeMI.Click += new System.EventHandler(this.getLastLockTimeMI_Click);
			// 
			// FrmMain
			// 
			this.AcceptButton = this.cmdAdd;
			this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 20F);
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.ClientSize = new System.Drawing.Size(461, 100);
			this.ContextMenuStrip = this.contextMenu;
			this.Controls.Add(this.label2);
			this.Controls.Add(this.lblEndTime);
			this.Controls.Add(this.lblDate);
			this.Controls.Add(this.lblTotal);
			this.Controls.Add(this.label4);
			this.Controls.Add(this.label3);
			this.Controls.Add(this.lblSpent);
			this.Controls.Add(this.cmdAdd);
			this.Controls.Add(this.chkLogged);
			this.Controls.Add(this.txtTime);
			this.Controls.Add(this.txtTask);
			this.Controls.Add(this.txtIniTime);
			this.Controls.Add(this.label1);
			this.Font = new System.Drawing.Font("Trebuchet MS", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
			this.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
			this.MaximizeBox = false;
			this.Name = "FrmMain";
			this.Text = "TimeLogger";
			this.contextMenu.ResumeLayout(false);
			this.ResumeLayout(false);
			this.PerformLayout();

		}

		#endregion

		private System.Windows.Forms.Label label1;
		private System.Windows.Forms.TextBox txtIniTime;
		private System.Windows.Forms.TextBox txtTask;
		private System.Windows.Forms.TextBox txtTime;
		private System.Windows.Forms.CheckBox chkLogged;
		private System.Windows.Forms.Button cmdAdd;
		private System.Windows.Forms.Label lblSpent;
		private System.Windows.Forms.Label label3;
		private System.Windows.Forms.Label label4;
		private System.Windows.Forms.Label lblTotal;
		private System.Windows.Forms.ContextMenuStrip contextMenu;
		private System.Windows.Forms.ToolStripMenuItem alwaysOnTopMI;
		private System.Windows.Forms.ToolStripMenuItem suggestOldTasksMI;
		private System.Windows.Forms.ToolStripMenuItem summarizeMI;
		private System.Windows.Forms.ToolStripMenuItem dummyToolStripMenuItem;
		private System.Windows.Forms.ToolStripMenuItem showChekboxesMI;
		private System.Windows.Forms.ToolStripMenuItem persistanceMI;
		private System.Windows.Forms.ToolStripMenuItem loadMI;
		private System.Windows.Forms.ToolStripMenuItem saveMI;
		private System.Windows.Forms.OpenFileDialog openFileDialog;
		private System.Windows.Forms.NotifyIcon notifyIcon;
		private System.Windows.Forms.Label lblDate;
		private System.Windows.Forms.Label lblEndTime;
		private System.Windows.Forms.Label label2;
		private System.Windows.Forms.ToolStripMenuItem getLastLockTimeMI;
	}
}